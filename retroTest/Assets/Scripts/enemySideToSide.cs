﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SynchronizerData;

public class enemySideToSide : MonoBehaviour {

	private BeatObserver beatObserver;
	private float leftOrRight; 
	public Sprite movingRight; 
	public Sprite movingLeft; 
	public float numberLanes;
	public float offset = 0;

	// Use this for initialization
	void Start () {
		/*
		Scene currentScene = SceneManager.GetActiveScene ();
		string sceneName = currentScene.name;

		if (sceneName == "single7") 
		{
			beatLength = 0.55f; 
		}
		else 
		{
			beatLength = 0.909f; 
		}

		beatTimer = beatLength;
*/
		beatObserver = GetComponent<BeatObserver>();
	}

	// Update is called once per frame
	void Update () {
		if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat && gameObject.GetComponent<collectMovement>().currentlyActive) {
			if (leftOrRight == 0) { 
				transform.Translate (new Vector3 (3f, 0f, 0f));
				SpriteRenderer movementSprite = GetComponentInChildren<SpriteRenderer>();
				movementSprite.sprite = movingLeft;
				leftOrRight = 1; 
			} else if (leftOrRight == 1) {
				transform.Translate (new Vector3 (-3f, 0f, 0f));
				SpriteRenderer movementSprite = GetComponentInChildren<SpriteRenderer>();
				movementSprite.sprite = movingRight; 
				leftOrRight = 0; 
			}
		}
	}

	public void initalizeEnemyMovement() {
		if (transform.position.x == -3 + offset) {
			SpriteRenderer movementSprite = GetComponentInChildren<SpriteRenderer> ();
			movementSprite.sprite = movingRight;
			leftOrRight = 0; 
		} else if (transform.position.x == (numberLanes * 3) - 6 + offset) {
			SpriteRenderer movementSprite = GetComponentInChildren<SpriteRenderer> ();
			movementSprite.sprite = movingLeft; 
			leftOrRight = 1; 
		} else {
			int rand = Random.Range (0, 2);
			if (rand == 0) { 
				SpriteRenderer movementSprite = GetComponentInChildren<SpriteRenderer> ();
				movementSprite.sprite = movingRight; 
				leftOrRight = 0; 
			} else {
				SpriteRenderer movementSprite = GetComponentInChildren<SpriteRenderer> ();
				movementSprite.sprite = movingLeft;
				leftOrRight = 1;
			}
		}
	}
}