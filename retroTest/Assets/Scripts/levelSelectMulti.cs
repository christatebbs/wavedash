﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class levelSelectMulti : MonoBehaviour {

	public Button level1;
	public Button level2;
	public Button level3;
	public Button back; 

	public static string curScene;
	public static int finalScoreOne;
	public static int finalScoreTwo;

	// Use this for initialization
	void Start () {
		level1.onClick.AddListener(loadLevelOne);
		level2.onClick.AddListener(loadLevelTwo);
		level3.onClick.AddListener (loadLevelThree);
		back.onClick.AddListener (loadMenu); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void loadLevelOne() {
		curScene = "multi3";
		SceneManager.LoadScene ("multi3");
	}
	void loadLevelTwo() {
		curScene = "multi4";
		SceneManager.LoadScene ("multi4");
	}
	void loadLevelThree() {
		curScene = "multi5";
		SceneManager.LoadScene ("multi5");
	}

	void loadMenu(){ 
		SceneManager.LoadScene ("menu"); 
	} 
}
