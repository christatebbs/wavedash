﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class displayFinalScoreMulti : MonoBehaviour {

	public Text score1;
	public Text score2;

	public Button restart;
	public Button menu;

	// Use this for initialization
	void Start () {
		score1.text = levelSelectMulti.finalScoreOne.ToString();
		score2.text = levelSelectMulti.finalScoreTwo.ToString();
		restart.onClick.AddListener(restartScene);
		menu.onClick.AddListener(loadMenu);
	}

	// Update is called once per frame
	void Update () {

	}

	void restartScene() {
		SceneManager.LoadScene (levelSelectMulti.curScene);
	}

	void loadMenu() {
		SceneManager.LoadScene ("menu");
	}
}
