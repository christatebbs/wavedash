﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class levelSelectSingle : MonoBehaviour {

	public Button level1;
	public Button level2;
	public Button level3;
	public Button level4;
	public Button level5;
	public Button back; 

	public Toggle endless;

	public static string curScene;
	public static int finalScore;

	// Use this for initialization
	void Start () {
		level1.onClick.AddListener(loadLevelOne);
		level2.onClick.AddListener(loadLevelTwo);
		level3.onClick.AddListener (loadLevelThree);
		level4.onClick.AddListener (loadLevelFour);
		level5.onClick.AddListener (loadLevelFive);
		back.onClick.AddListener (loadMenu); 
	}

	void loadLevelOne() {
		if (endless.isOn) {
			curScene = "endless3";
			SceneManager.LoadScene ("endless3");
		} else {
			curScene = "single3";
			SceneManager.LoadScene ("single3");
		}
	}
	void loadLevelTwo() {
		if (endless.isOn) {
			curScene = "endless4";
			SceneManager.LoadScene ("endless4");
		} else {
			curScene = "single4";
			SceneManager.LoadScene ("single4");
		}
	}
	void loadLevelThree() {
		if (endless.isOn) {
			curScene = "endless5";
			SceneManager.LoadScene ("endless5");
		} else {
			curScene = "single5";
			SceneManager.LoadScene ("single5");
		}
	}
	void loadLevelFour() {
		if (endless.isOn) {
			curScene = "endless6";
			SceneManager.LoadScene ("endless6");
		} else {
			curScene = "single6";
			SceneManager.LoadScene ("single6");
		}
	}
	void loadLevelFive() {
		if (endless.isOn) {
			curScene = "endless7";
			SceneManager.LoadScene ("endless7");
		} else {
			curScene = "single7";
			SceneManager.LoadScene ("single7");
		}
	}

	void loadMenu() { 
		SceneManager.LoadScene ("menu"); 
	}
	// Update is called once per frame
	void Update () {
		
	}
}
