﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuControl : MonoBehaviour {

	public Button single;
	public Button multi;
	public Button inst;
	// Use this for initialization
	void Start () {
		single.onClick.AddListener(loadSingle);
		multi.onClick.AddListener(loadMulti);
		inst.onClick.AddListener (loadInst);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void loadInst() {
		SceneManager.LoadScene ("instructions");
	}

	void loadSingle() {
		SceneManager.LoadScene ("singleLevelSelect");
	}

	void loadMulti() {
		SceneManager.LoadScene ("multiLevelSelect");

	}	
}
