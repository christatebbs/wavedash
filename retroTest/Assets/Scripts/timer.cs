﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SynchronizerData;

public class timer : MonoBehaviour {

	private Color startColor = Color.white;
	private Color endColor = Color.black;
	private float lerpLength = 1.0f;
	private float lerpTimer;
	private BeatObserver beatObserver;
	public GameObject boombox; 
	private Animator anim; 

	private void Start() {
		beatObserver = GetComponent<BeatObserver>();
		anim = boombox.GetComponent<Animator> ();
	}

	private void Update()
	{
		if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat) {
			lerpTimer = 0f;
			anim.Play ("Boom", 0, 0.4f); 
		}
		lerpTimer += Time.deltaTime / lerpLength;
	}
}
