﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadMenu : MonoBehaviour {

	public Button menuBtn;
	// Use this for initialization
	void Start () {
		menuBtn.onClick.AddListener (loadMenu);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void loadMenu() {
		SceneManager.LoadScene ("menu");
	}
}
