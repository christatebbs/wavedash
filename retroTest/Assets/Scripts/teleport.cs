﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SynchronizerData;

public class teleport : MonoBehaviour {
	private GameObject target;
	private BeatObserver beatObserver;
	public int numberLanes; 
	private float currentLane;
	private float targetLane;
	private float safePath;
	public bool singlePlayer = true;

	// Use this for initialization
	void Start () {
		/*
		Scene currentScene = SceneManager.GetActiveScene ();
		string sceneName = currentScene.name;

		if (sceneName == "single7") 
		{
			beatLength = 0.55f; 
		}
		else 
		{
			beatLength = 0.909f; 
		}
*/
		target = transform.GetChild (1).gameObject;
		beatObserver = GetComponent<BeatObserver>();
	}

	// Update is called once per frame
	void Update () {
		if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat && gameObject.GetComponent<collectMovement>().currentlyActive) {
			transform.Translate (new Vector3((targetLane - currentLane) * 3, 0, 0));
			int randomNum = Random.Range(0, numberLanes);
			if (transform.position.z == -12f && !singlePlayer) {
				while (randomNum == targetLane || randomNum == safePath) {
					randomNum = Random.Range (0, numberLanes); 
				}
			} else {
				while (randomNum == targetLane) {
					randomNum = Random.Range (0, numberLanes); 
				}
			}
			currentLane = targetLane;
			targetLane = randomNum;
			target.transform.position = transform.position;
			target.transform.Translate (new Vector3 ((targetLane - currentLane) * 3, 0, 0));
		}
	}

	public void initalizeEnemyMovement(int startLane) {
		currentLane = startLane;
		int randomNum = Random.Range(0, numberLanes);
		while (randomNum == currentLane) { 
			randomNum = Random.Range (0, numberLanes); 
		} 
		targetLane = randomNum;
		target.transform.position = transform.position;
		target.transform.Translate (new Vector3((targetLane - currentLane) * 3, 0, 0));
	}
}
