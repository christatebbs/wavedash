﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class displayFinalScore : MonoBehaviour {

	public Text scoreText;

	public Button restart;
	public Button menu;

	// Use this for initialization
	void Start () {
		scoreText.text = levelSelectSingle.finalScore.ToString();
		restart.onClick.AddListener(restartScene);
		menu.onClick.AddListener(loadMenu);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void restartScene() {
		SceneManager.LoadScene (levelSelectSingle.curScene);
	}

	void loadMenu() {
		SceneManager.LoadScene ("menu");
	}
}
