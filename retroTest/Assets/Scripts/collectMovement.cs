﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SynchronizerData;
using UnityEngine.SceneManagement;

public class collectMovement : MonoBehaviour {

	private BeatObserver beatObserver;
	private spawnBehavior spawner;
	private spawnBehaviorFile spawnerFile;
	public bool currentlyActive = false;
	private bool singlePlayer;
	// Use this for initialization
	void Start () {
		beatObserver = GetComponent<BeatObserver>();
		if (SceneManager.GetActiveScene ().name.Equals ("single3") ||
		    SceneManager.GetActiveScene ().name.Equals ("single4") ||
		    SceneManager.GetActiveScene ().name.Equals ("single5") ||
		    SceneManager.GetActiveScene ().name.Equals ("single6") ||
		    SceneManager.GetActiveScene ().name.Equals ("single7")) {
			spawnerFile = GameObject.Find ("laneSpawner").GetComponent<spawnBehaviorFile> ();
			singlePlayer = true;
		} else {
			spawner = GameObject.Find ("laneSpawner").GetComponent<spawnBehavior> ();
			singlePlayer = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.z < -15f) {
			if (singlePlayer) {
				if (CompareTag ("enemy")) {
					spawnerFile.availableEnemies.Add (gameObject);
				} else if (CompareTag ("health")) {
					spawnerFile.availableHealth.Add (gameObject);
				} else if (CompareTag ("laser")) {
					spawnerFile.availableLasers.Add (gameObject);
				} else if (CompareTag ("shield")) {
					spawnerFile.availableShields.Add (gameObject);
				} else if (CompareTag ("scores1")) {
					spawnerFile.availableScores1.Add (gameObject);
				} else if (CompareTag ("scores2")) {
					spawnerFile.availableScores2.Add (gameObject);
				} else if (CompareTag ("scores3")) {
					spawnerFile.availableScores3.Add (gameObject);
				} else if (CompareTag ("movingEnemy")) {
					spawnerFile.availableMovingEnemies.Add (gameObject);
				} else if (CompareTag ("sideToSideEnemy")) {
					spawnerFile.availableSideEnemies.Add (gameObject);
				}
			} else {
				if (CompareTag ("enemy")) {
					spawner.availableEnemies.Add (gameObject);
				} else if (CompareTag ("health")) {
					spawner.availableHealth.Add (gameObject);
				} else if (CompareTag ("laser")) {
					spawner.availableLasers.Add (gameObject);
				} else if (CompareTag ("shield")) {
					spawner.availableShields.Add (gameObject);
				} else if (CompareTag ("scores1")) {
					spawner.availableScores1.Add (gameObject);
				} else if (CompareTag ("scores2")) {
					spawner.availableScores2.Add (gameObject);
				} else if (CompareTag ("scores3")) {
					spawner.availableScores3.Add (gameObject);
				} else if (CompareTag ("movingEnemy")) {
					spawner.availableMovingEnemies.Add (gameObject);
				} else if (CompareTag ("sideToSideEnemy")) {
					spawner.availableSideEnemies.Add (gameObject);
				}
			}
			currentlyActive = false;
			transform.position = GameObject.Find ("Holding Location").transform.position;
		}

		if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat && currentlyActive) {
			transform.Translate (new Vector3 (0f, 0f, -3f));
			//Debug.Log (transform.position.z);
		}
	}

	public void initalizeMovement() {
		transform.rotation = Quaternion.identity;
		transform.GetChild(0).rotation = Quaternion.identity;
		if (CompareTag ("movingEnemy")) {
			transform.GetChild(1).GetChild(0).rotation = Quaternion.identity;
		}
		//currentlyActive = true;
	}
}
