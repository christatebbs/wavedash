﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SynchronizerData;
using UnityEngine.SceneManagement;
using System.Linq;

public class spawnBehaviorFile : MonoBehaviour {

	private BeatObserver beatObserver;
	private string[] spawnOrder;
	private int curLength = 0;
	private int numberLanes = 3;

	public List<GameObject> availableEnemies;
	public List<GameObject> availableSideEnemies;
	public List<GameObject> availableMovingEnemies;
	public List<GameObject> availableHealth;
	public List<GameObject> availableLasers;
	public List<GameObject> availableShields;
	public List<GameObject> availableScores1;
	public List<GameObject> availableScores2;
	public List<GameObject> availableScores3;
	// Use this for initialization
	void Start () {

		availableEnemies = GameObject.FindGameObjectsWithTag ("enemy").ToList();
		availableHealth = GameObject.FindGameObjectsWithTag ("health").ToList();
		availableLasers = GameObject.FindGameObjectsWithTag ("laser").ToList();
		availableShields = GameObject.FindGameObjectsWithTag ("shield").ToList();
		availableScores1 = GameObject.FindGameObjectsWithTag ("scores1").ToList();
		availableScores2 = GameObject.FindGameObjectsWithTag ("scores2").ToList();
		availableScores3 = GameObject.FindGameObjectsWithTag ("scores3").ToList();
		availableSideEnemies = GameObject.FindGameObjectsWithTag ("sideToSideEnemy").ToList();
		availableMovingEnemies = GameObject.FindGameObjectsWithTag ("movingEnemy").ToList();

		GameObject downObject = GameObject.Find ("down_beat_counter");
		GameObject offObject = GameObject.Find ("off_beat_counter");
		GameObject patternObject = GameObject.Find ("pattern_counter");
		GameObject upObject = GameObject.Find ("up_beat_counter");
		BeatCounter downBeat = downObject.GetComponent<BeatCounter> ();
		BeatCounter offBeat = offObject.GetComponent<BeatCounter> ();
		PatternCounter patternBeat = patternObject.GetComponent<PatternCounter> ();
		BeatCounter upBeat = upObject.GetComponent<BeatCounter> ();

		GameObject[] tempObservers = new GameObject[availableEnemies.Count + availableSideEnemies.Count + availableMovingEnemies.Count + availableHealth.Count + availableLasers.Count + availableShields.Count +
			availableScores1.Count + availableScores2.Count + availableScores3.Count + 3];

		tempObservers [0] = GameObject.Find ("Player");
		tempObservers [1] = GameObject.Find ("laneSpawner");
		tempObservers [2] = GameObject.Find ("timer");
		List<GameObject> tempList = availableEnemies;
		tempList.AddRange (availableHealth);
		tempList.AddRange (availableSideEnemies);
		tempList.AddRange (availableMovingEnemies);
		tempList.AddRange (availableLasers);
		tempList.AddRange (availableShields);
		tempList.AddRange (availableScores1);
		tempList.AddRange (availableScores2);
		tempList.AddRange (availableScores3);

		for (int i = 0; i < tempObservers.Length - 3; i++) {
			tempObservers [i + 3] = tempList [0];
			tempList.RemoveAt (0);
		}

		availableEnemies = GameObject.FindGameObjectsWithTag ("enemy").ToList();
		availableHealth = GameObject.FindGameObjectsWithTag ("health").ToList();
		availableLasers = GameObject.FindGameObjectsWithTag ("laser").ToList();
		availableShields = GameObject.FindGameObjectsWithTag ("shield").ToList();
		availableScores1 = GameObject.FindGameObjectsWithTag ("scores1").ToList();
		availableScores2 = GameObject.FindGameObjectsWithTag ("scores2").ToList();
		availableScores3 = GameObject.FindGameObjectsWithTag ("scores3").ToList();
		availableSideEnemies = GameObject.FindGameObjectsWithTag ("sideToSideEnemy").ToList();
		availableMovingEnemies = GameObject.FindGameObjectsWithTag ("movingEnemy").ToList();

		downBeat.observers = tempObservers;
		offBeat.observers = tempObservers;
		patternBeat.observers = tempObservers;
		upBeat.observers = tempObservers;


		//*****************************************************************//
		// Put your strings here
//		0 = blanks
//		1 = score1
//		2 = score2
//		3 = score3
//		4 = enemy
//		5 = health
//		6 = shield
//		7 = laser
//		8 = moving
//		9 = side to side
		if (SceneManager.GetActiveScene ().name.Equals ("single3")) {
			numberLanes = 3;
			spawnOrder = new string[] { "010","200","003","404","505","060","100","400","007","102","301","900","009","201",
				"103","080","105","306","123","800","321","090","504","046","703","350","900","703","620","008","090","104",
				"402","050","603","702","090","800","016","502","462","174","800","123","090","321","080","152","040","362",
				"090","271","080","153","090","402","340","105","403","047","010","101","040","403","604","050","240","043",
				"506","070","302","404","454","800","123","321","647","044","104","620","430","900","042","320","154","721",
				"326","401","500","123","040","151","373","262","903","201","309","102","563","725","404","404","030","040",
				"050","030","202","010","008","321","800","123","008","010","800","020","008","030","800","404","008","303"};

		} else if (SceneManager.GetActiveScene ().name.Equals ("single4")) {
			numberLanes = 4;
			spawnOrder = new string[] { "1111","2222","3333","0405","0504","9007","6008","7006","2441","1443","5005","4314",
				"2442","1111","2431","6241","4674","4434","5090","1057","0201","3060","0705","4404","4040","4345","6666",
				"0000","4004","3443","4567","1234","4321","0000","5471","6724","4201","0142","4034","9005","3204","8057",
				"6108","9103","3412","4217","5005","4444","4321","1234","6172","8009","9008","3002","1007","9009","6005",
				"8008","3610","7025","3501","0264","4208","2443","4214","8042","0905","4051","3080","6413","4403","1044",
				"5471","0801","4341","2464","0000","1234","4321","9012","0308","5471","3409","4671","1576","4444","1609",
				"9005","4334","1462","6173","1111","4090","0804","7009","8006","4554","3442","7446","5443","4021","2081",
				"3752","0306","7010","3190","0821","6753","3576","4004","4004","5555","1234","4321","6092","1803","0080",
				"0900","4444","1111","2222","3333"};

		} else if (SceneManager.GetActiveScene ().name.Equals ("single5")) {
			numberLanes = 5;
			spawnOrder = new string[] { 
			"11111", "14341", "44644" , "40900", "09544", "37422", "50111" , "00800" , "90709" , 
			"44344", "90409", "11111", "14341", "44644", "11111", "14341", "06060", "40444", "423423", "00000", "90009", 
			"08000" , "71444" , "41245" , "33090" , "12211" , "44234", "00000", "02244" , "12255", "24277", "09544", "37422", "50111" ,  
			"14004" , "00000", "11111", "14341", "44644" , "40904", "09544", "37422", "50111" , "00800" ,  "09544", "37422", 
			"50111" , "00800" , "90709" , "14709" , "11001" , "30021" , "14441" , "44009" , "11244" , "04040" , "00000" , "09000",
			"44344", "90409" , "11111", "14341", "44644" , "11111", "14341", "06660" , "44444" , "43423", "00000", "90009" , 
			"08000" , "71344" , "41245" , "33090" , "12211" , "44234", "00000" , "13131" , "00000" , "12344" , "11111" , "33333", "12121"};

		} else if (SceneManager.GetActiveScene ().name.Equals ("single6")) {
			numberLanes = 6;
			spawnOrder = new string[] { 
				"102010", "100021", "440433", "222222", "000000", "900904", "324233", "123214", "123455",
				"123144", "000000", "006600", "444444", "700090", "033004", "443354", "900444", "000000", "900004", "433244", 
				"444433", "324233", "123214", "123455", "123144", "111111", "111121", "444433", "324233", "123214", "123455", 
				"123144", "324233", "123214", "123455", "123144", "111111", "111121", "444433", "324233", "123214", "123455", 
				"123144", "111111", "111121", "444433", "324233", "123214", "123455", "123144", "111111", "111121", "444433",
				"324233", "123214", "123455", "123144", "102010", "100021", "440433", "222222", "000000", "909044", "324233",
				"123214", "123455", "123144", "111111", "111121", "444433", "324233", "123214", "123455", "000800", "900900", 
				"000000", "111121", "444433", "324233", "123214", "123455", "123144", "111111", "111121", "444433", "324233", 
				"123214", "123455", "123144", "324233", "123214", "123455", "123144", "111111", "111121", "444433", "324233", 
				"123214", "123455", "123144", "111111", "111121", "444433", "324233", "123214", "123455", "123144", "111111", 
				"111121", "444433", "324233", "123214", "123455", "123144", "102010", "100021", "440433", "222222", "000000",
				"100021", "440433", "222222"};
		} else if (SceneManager.GetActiveScene ().name.Equals ("single7")) {
			numberLanes = 7;
				//0 = blanks
				//		1 = score1
				//		2 = score2
				//		3 = score3
				//		4 = enemy
				//		5 = health
				//		6 = shield
				//		7 = laser
				//		8 = moving
				//		9 = side to side

			spawnOrder = new string[] { 
				"1404040",
				"4144404",
				"1404240",
				"4144404",
				"0414040",
				"4440424",
				"0404044",
				"4044404",
				"0404040",
				"4044404",
				"0404040",
				"4040404",
				"3440443",
				"1440440", 
				"1469340", 
				"0429041", 
				"0439641", 
				"0409542", 
				"0344404", 
				"0444044", 
				"0440444", 
				"0404044", 
				"4244414", 
				"4344434",
				"1917191",
				"2010031",
				"1200100",
				"8001017",
				"0083600",
				"1111118",
				"7930440",
				"0400301",
				"0080000",
				"4440444",
				"0000000",
				"0400040",
				"5900907",
				"4446444",
				"4444444",
				"3004044",
				"0054404",
				"0444440",
				"0044443",
				"0404443",
				"4040443",
				"2414543",
				"4044044",
				"3920444",
				"0404444",
				"0409544",
				"5404404",
				"4044440",
				"1419114",
				"8001013",
				"1404040",
				"4144404",
				"1404240",
				"4144404",
				"0414040",
				"4440424",
				"0404044",
				"4044404",
				"0404040",
				"4044404",
				"4404044",
				"4040404",
				"3440443",
				"1440440", 
				"1469340", 
				"0429041", 
				"0439641", 
				"0409542", 
				"0344404",
				"0060500",
				"9045444",
				"0044465",
				"1123333",
				"4404444",
				"0000000",
				"4444044",
				"0000000",
				"4404444",
				"4041033",
				"0444044",
				"1934404",
				"9010433",
				"2821011",
				"0008111",
				"4144414",
				"0008111",
				"4144414",
				"0008111",
				"4144414",
				"2821011",
				"0008111",
				"4144414",
				"0008111",
				"4144414",
				"0008111",
				"4144414",
				"0008111",
				"0008111",
				"0008111",
				"0008111",
				"0008111"

				};
			//0 = blanks
			//		1 = score1
			//		2 = score2
			//		3 = score3
			//		4 = enemy
			//		5 = health
			//		6 = shield
			//		7 = laser
			//		8 = moving
			//		9 = side to side
		}

		beatObserver = GetComponent<BeatObserver>();
		//*******************************************************************//
	}
	
	// Update is called once per frame
	void Update () {
		if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat) {
			char[] cur = spawnOrder [curLength].ToCharArray ();
			for (int i = 0; i < numberLanes; i++) {
				if (cur [i].Equals ('7')) {
					GameObject curGo = availableLasers [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableLasers.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
				} else if (cur [i].Equals ('6')) {
					GameObject curGo = availableShields [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableShields.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
				} else if (cur [i].Equals ('5')) {
					GameObject curGo = availableHealth [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableHealth.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
				} else if (cur [i].Equals ('4')) {
					//int rand = Random.Range (0, availableEnemies.Count);
					GameObject curGo = availableEnemies [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableEnemies.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
				} else if (cur [i].Equals ('3')) {
					GameObject curGo = availableScores3 [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
					availableScores3.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (cur [i].Equals ('2')) {
					GameObject curGo = availableScores2 [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
					availableScores2.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (cur [i].Equals ('1')) {
					GameObject curGo = availableScores1 [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
					availableScores1.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (cur [i].Equals ('8')) { 
					GameObject curGo = availableMovingEnemies [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
					availableMovingEnemies.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
					curGo.GetComponent<teleport> ().initalizeEnemyMovement (i);
				} else if (cur [i].Equals ('9')) { 
					GameObject curGo = availableSideEnemies [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					curGo.GetComponent<collectMovement> ().initalizeMovement ();
					availableSideEnemies.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
					curGo.GetComponent<enemySideToSide> ().initalizeEnemyMovement ();
				}
			}
			curLength++;
		}
	}
}
