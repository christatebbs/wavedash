﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class instructionBack : MonoBehaviour {

	public Button back;
	// Use this for initialization
	void Start () {
		back.onClick.AddListener(loadMenu);
	}

	void loadMenu() {
		SceneManager.LoadScene ("menu");
	}	
}