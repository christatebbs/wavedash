using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SynchronizerData;

public class MoveLeftAndRightFile : MonoBehaviour {
	
	//private double beatLength = 60.0 / 66.0;
	//private double beatTimer;
	private int detectedInput = 0;
	public int trackLength;
	public Text numLives2; 
	public Text count2;
	public Text numLives; 
	public Text count;
	public Text finalScore;
	public Text finalScore2;
	public Button menu;
	public SpriteRenderer playerSprite;
	public GameObject playerShield; 
	public Sprite neutralSprite;
	public Sprite rightSprite; 
	public Sprite leftSprite; 
	private float score = 0;
	private float lives = 1;
	private float numberOfLasers = 0;
	private bool shieldEquipped = false;
	public int playerOffset;
	public int playerNumber = 1;
	private bool keyHeld = false; 
	private BeatObserver beatObserver;
	private spawnBehaviorFile spawner;

	// Use this for initialization
	void Start () {
		//beatTimer = beatLength;
		numLives.text = lives.ToString ();
		spawner = GameObject.Find ("laneSpawner").GetComponent<spawnBehaviorFile> ();
		beatObserver = GetComponent<BeatObserver>();
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log (lives);
		if (lives != 0) {
			if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat) {
				if (detectedInput == 1 && transform.position.x < ((trackLength - 2) * 3) + playerOffset) {
					transform.Translate (new Vector3 (3f, 0f, 0f));
				} else if (detectedInput == 2 && transform.position.x > -3 + playerOffset) {
					transform.Translate (new Vector3 (-3f, 0f, 0f));
				}
			}
			GameObject[] shield = GameObject.FindGameObjectsWithTag ("shield");
			for (int i = 0; i < shield.Length; i++) {
				if (Vector3.Distance(shield [i].transform.position, transform.position) < 1) {
					shield[i].GetComponent<collectMovement> ().currentlyActive = false;
					shield[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("gotShield")); 
					spawner.availableShields.Add (shield[i]);
					//playerSprite.sprite = shieldSprite;
					playerShield.SetActive(true); 
					shieldEquipped = true;
				}
			}
			GameObject[] health = GameObject.FindGameObjectsWithTag ("health");
			for (int i = 0; i < health.Length; i++) {
				if (Vector3.Distance(health [i].transform.position, transform.position) < 1) {
					health[i].GetComponent<collectMovement> ().currentlyActive = false;
					health[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("gotHealth")); 
					spawner.availableHealth.Add (health[i]);
					lives++;
					numLives.text = lives.ToString (); 
				}
			}
			GameObject[] scores1 = GameObject.FindGameObjectsWithTag ("scores1");
			for (int i = 0; i < scores1.Length; i++) {
				if (Vector3.Distance(scores1 [i].transform.position, transform.position) < 1) {
					scores1[i].GetComponent<collectMovement> ().currentlyActive = false;
					scores1[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					spawner.availableScores1.Add (scores1[i]);
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("coinCollect")); 
					score = score + 1;
					count.text = score.ToString ();
					Debug.Log ("hit");
				}
			}
			GameObject[] scores2 = GameObject.FindGameObjectsWithTag ("scores2");
			for (int i = 0; i < scores2.Length; i++) {
				if (Vector3.Distance(scores2 [i].transform.position, transform.position) < 1) {
					scores2[i].GetComponent<collectMovement> ().currentlyActive = false;
					scores2[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					spawner.availableScores2.Add (scores2[i]);
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("coinCollect")); 
					score = score + 2;
					count.text = score.ToString (); 
				}
			}
			GameObject[] scores3 = GameObject.FindGameObjectsWithTag ("scores3");
			for (int i = 0; i < scores3.Length; i++) {
				if (Vector3.Distance(scores3 [i].transform.position, transform.position) < 1) {
					scores3[i].GetComponent<collectMovement> ().currentlyActive = false;
					scores3[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("coinCollect"));
					spawner.availableScores3.Add (scores3[i]);
					score = score + 3;
					count.text = score.ToString (); 
				}
			}
			GameObject[] enemy = GameObject.FindGameObjectsWithTag ("enemy");
			for (int i = 0; i < enemy.Length; i++) {
				if (Vector3.Distance(enemy [i].transform.position, transform.position) < 1) {
					enemy[i].GetComponent<collectMovement> ().currentlyActive = false;
					enemy[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("hitEnemy"));
					spawner.availableEnemies.Add (enemy[i]);
					if (!shieldEquipped) {
						lives--;
						numLives.text = lives.ToString ();
					} else {
						shieldEquipped = false;
						playerShield.SetActive(false);  
						//playerSprite.sprite = neutralSprite;
					}
				}
			}
			GameObject[] sideEnemy = GameObject.FindGameObjectsWithTag ("sideToSideEnemy");
			for (int i = 0; i < sideEnemy.Length; i++) {
				if (Vector3.Distance(sideEnemy [i].transform.position, transform.position) < 1) {
					sideEnemy[i].GetComponent<collectMovement> ().currentlyActive = false;
					sideEnemy[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("hitEnemy"));
					spawner.availableEnemies.Add (sideEnemy[i]);
					if (!shieldEquipped) {
						lives--;
						numLives.text = lives.ToString ();
					} else {
						shieldEquipped = false;
						playerShield.SetActive(false);  
						//playerSprite.sprite = neutralSprite;
					}
				}
			}
			GameObject[] movingEnemy = GameObject.FindGameObjectsWithTag ("movingEnemy");
			for (int i = 0; i < movingEnemy.Length; i++) {
				if (Vector3.Distance(movingEnemy [i].transform.position, transform.position) < 1) {
					movingEnemy[i].GetComponent<collectMovement> ().currentlyActive = false;
					movingEnemy[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("hitEnemy"));
					spawner.availableMovingEnemies.Add (movingEnemy[i]);
					if (!shieldEquipped) {
						lives--;
						numLives.text = lives.ToString ();
					} else {
						shieldEquipped = false;
						playerShield.SetActive(false);  
						//playerSprite.sprite = neutralSprite;
					}
				}
			}
			GameObject[] laser = GameObject.FindGameObjectsWithTag ("laser");
			for (int i = 0; i < laser.Length; i++) {
				if (Vector3.Distance(laser [i].transform.position, transform.position) < 1) {
					laser[i].GetComponent<collectMovement> ().currentlyActive = false;
					laser[i].transform.position = GameObject.Find ("Holding Location").transform.position;
					spawner.availableEnemies.Add (laser[i]);
					AudioSource audio = gameObject.AddComponent<AudioSource> (); 
					audio.PlayOneShot ((AudioClip)Resources.Load ("gotLaser")); 
					if (numberOfLasers == 0) {
						transform.localScale = new Vector3(transform.localScale.x * 1.2f, transform.localScale.y * 1.2f, transform.localScale.z * 1.2f);
					}
					numberOfLasers++;
				}
			}
			if (playerNumber == 1 || playerNumber == 0) {
				if (Input.GetKeyDown ("d")) {
					playerSprite.sprite = rightSprite; 
					detectedInput = 1;
					keyHeld = true;
				}
				if (Input.GetKeyDown ("a")) {
					playerSprite.sprite = leftSprite; 
					detectedInput = 2;
					keyHeld = true;
				}
				if (Input.GetKeyDown ("s")) {
					if (numberOfLasers != 0) {
						numberOfLasers--;
						AudioSource audio = gameObject.AddComponent<AudioSource> (); 
						audio.PlayOneShot ((AudioClip)Resources.Load ("useLaser"));
						GameObject[] enemies = GameObject.FindGameObjectsWithTag ("enemy");
						for (int i = 1; i < 12; i++) {
							Vector3 lane = new Vector3 (transform.position.x, transform.position.y, (transform.position.z + (3 * i)));
							for (int j = 0; j < enemies.Length; j++) {
								if (Vector3.Distance (enemies [j].transform.position, lane) < 1) {
									enemies[j].GetComponent<collectMovement> ().currentlyActive = false;
									enemies[j].transform.position = GameObject.Find ("Holding Location").transform.position;
									spawner.availableEnemies.Add (enemy[j]);
									score++;
									count.text = score.ToString ();
								}
							}
						}
						GameObject[] sideEnemies = GameObject.FindGameObjectsWithTag ("sideToSideEnemy");
						for (int i = 0; i < sideEnemies.Length; i++) {
							if (Vector3.Distance(sideEnemies [i].transform.position, transform.position) < 1) {
								sideEnemies[i].GetComponent<collectMovement> ().currentlyActive = false;
								sideEnemies[i].transform.position = GameObject.Find ("Holding Location").transform.position;
								spawner.availableEnemies.Add (sideEnemies[i]);
								if (!shieldEquipped) {
									lives--;
									numLives.text = lives.ToString ();
								} else {
									shieldEquipped = false;
									playerShield.SetActive(false);  
									//playerSprite.sprite = neutralSprite;
								}
							}
						}
						GameObject[] movingEnemies = GameObject.FindGameObjectsWithTag ("movingEnemy");
						for (int i = 0; i < movingEnemies.Length; i++) {
							if (Vector3.Distance(movingEnemies [i].transform.position, transform.position) < 1) {
								movingEnemies[i].GetComponent<collectMovement> ().currentlyActive = false;
								movingEnemies[i].transform.position = GameObject.Find ("Holding Location").transform.position;
								spawner.availableMovingEnemies.Add (movingEnemies[i]);
								if (!shieldEquipped) {
									lives--;
									numLives.text = lives.ToString ();
								} else {
									shieldEquipped = false;
									playerShield.SetActive(false);  
									//playerSprite.sprite = neutralSprite;
								}
							}
						}
						if (numberOfLasers == 0) {
							transform.localScale = new Vector3 (transform.localScale.x * 0.8f, transform.localScale.y * 0.8f, transform.localScale.z * 0.8f);
						}
					}
				}
				if (Input.GetKeyUp ("d") && detectedInput == 1) {
					playerSprite.sprite = neutralSprite; 
					detectedInput = 0;
					keyHeld = false;
				}
				if (Input.GetKeyUp ("a") && detectedInput == 2) {
					playerSprite.sprite = neutralSprite;
					detectedInput = 0;
					keyHeld = false;
				}
			}
			if (playerNumber == 2 || playerNumber == 0) {
				if (Input.GetKeyDown (KeyCode.RightArrow)) {
					playerSprite.sprite = rightSprite; 
					detectedInput = 1;
					keyHeld = true;
				}
				if (Input.GetKeyDown (KeyCode.LeftArrow)) {
					playerSprite.sprite = leftSprite; 
					detectedInput = 2;
					keyHeld = true;
				}
				if (Input.GetKeyDown (KeyCode.DownArrow)) {
					if (numberOfLasers != 0) {
						numberOfLasers--;
						AudioSource audio = gameObject.AddComponent<AudioSource> (); 
						audio.PlayOneShot ((AudioClip)Resources.Load ("useLaser"));
						GameObject[] enemies = GameObject.FindGameObjectsWithTag ("enemy");
						for (int i = 1; i < 12; i++) {
							Vector3 lane = new Vector3 (transform.position.x, transform.position.y, (transform.position.z + (3 * i)));
							for (int j = 0; j < enemies.Length; j++) {
								if (Vector3.Distance (enemies [j].transform.position, lane) < 1) {
									enemies[j].GetComponent<collectMovement> ().currentlyActive = false;
									enemies[j].transform.position = GameObject.Find ("Holding Location").transform.position;
									spawner.availableEnemies.Add (enemy[j]);
									score++;
									count.text = score.ToString (); 
								}
							}
						}
						GameObject[] sideEnemies = GameObject.FindGameObjectsWithTag ("sideToSideEnemy");
						for (int i = 0; i < sideEnemies.Length; i++) {
							if (Vector3.Distance(sideEnemies [i].transform.position, transform.position) < 1) {
								sideEnemies[i].GetComponent<collectMovement> ().currentlyActive = false;
								sideEnemies[i].transform.position = GameObject.Find ("Holding Location").transform.position;
								spawner.availableEnemies.Add (sideEnemies[i]);
								AudioSource a = gameObject.AddComponent<AudioSource> (); 
								audio.PlayOneShot ((AudioClip)Resources.Load ("hitEnemy"));
								if (!shieldEquipped) {
									lives--;
									numLives.text = lives.ToString ();
								} else {
									shieldEquipped = false;
									playerShield.SetActive(false);  
									//playerSprite.sprite = neutralSprite;
								}
							}
						}
						GameObject[] movingEnemies = GameObject.FindGameObjectsWithTag ("movingEnemy");
						for (int i = 0; i < movingEnemies.Length; i++) {
							if (Vector3.Distance(movingEnemies [i].transform.position, transform.position) < 1) {
								movingEnemies[i].GetComponent<collectMovement> ().currentlyActive = false;
								movingEnemies[i].transform.position = GameObject.Find ("Holding Location").transform.position;
								spawner.availableMovingEnemies.Add (movingEnemies[i]);
								if (!shieldEquipped) {
									lives--;
									numLives.text = lives.ToString ();
								} else {
									shieldEquipped = false;
									playerShield.SetActive(false);  
									//playerSprite.sprite = neutralSprite;
								}
							}
						}
						if (numberOfLasers == 0) {
							transform.localScale = new Vector3 (transform.localScale.x * 0.8f, transform.localScale.y * 0.8f, transform.localScale.z * 0.8f);
						}
					}
				}
				if (Input.GetKeyUp (KeyCode.RightArrow) && detectedInput == 1) {
					playerSprite.sprite = neutralSprite; 
					detectedInput = 0;
					keyHeld = false;
				}
				if (Input.GetKeyUp (KeyCode.LeftArrow) && detectedInput == 2) {
					playerSprite.sprite = neutralSprite; 
					detectedInput = 0;
					keyHeld = false;
				}
			}
		} else {
			finalScore2.gameObject.SetActive (true);
			count2.gameObject.SetActive (false);
			numLives2.gameObject.SetActive (false);
			menu.onClick.AddListener(loadMenu);
			finalScore.text = "Final Score: " + score.ToString ();
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			loadMenu ();
		}
	}

	void loadMenu() {
		SceneManager.LoadScene ("menu");
	}
}
