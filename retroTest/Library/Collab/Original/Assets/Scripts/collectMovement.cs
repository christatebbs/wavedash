﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SynchronizerData;

public class collectMovement : MonoBehaviour {

	private BeatObserver beatObserver;
	private spawnBehavior spawner;
	public bool currentlyActive = false;
	// Use this for initialization
	void Start () {
		beatObserver = GetComponent<BeatObserver>();
		spawner = GameObject.Find ("laneSpawner").GetComponent<spawnBehavior> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.z < -15f) {
			if (CompareTag ("enemy")) {
				spawner.availableEnemies.Add (gameObject);
			} else if (CompareTag ("health")) {
				spawner.availableHealth.Add (gameObject);
			} else if (CompareTag ("laser")) {
				spawner.availableLasers.Add (gameObject);
			} else if (CompareTag ("shield")) {
				spawner.availableShields.Add (gameObject);
			} else if (CompareTag ("scores1")) {
				spawner.availableScores1.Add (gameObject);
			} else if (CompareTag ("scores2")) {
				spawner.availableScores2.Add (gameObject);
			} else if (CompareTag ("scores3")) {
				spawner.availableScores3.Add (gameObject);
			}
			currentlyActive = false;
			gameObject.transform.position = GameObject.Find ("Holding Location").transform.position;
		}

		if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat && currentlyActive) {
			transform.Translate (new Vector3 (0f, 0f, -3f));
			transform.rotation = new Vector3 (transform.rotation.x + 0.1f, 0f, 0f); 
		}
	}
}
