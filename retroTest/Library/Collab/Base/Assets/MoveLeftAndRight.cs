﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeftAndRight : MonoBehaviour {
	
	public float beatLength = 1.0f;
	private int detectedInput = 0;
	public float trackLength = 3f;
	private float score = 0;
	private float lives = 1;
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		GameObject[] health = GameObject.FindGameObjectsWithTag ("health");
		for (int i = 0; i < health.Length; i++) {
			if (health [i].transform.position == transform.position) {
				Destroy (health [i]);
				lives++;
			}
		}
		GameObject[] scores = GameObject.FindGameObjectsWithTag ("scores");
		for (int i = 0; i < scores.Length; i++) {
			if (scores [i].transform.position == transform.position) {
				Destroy (scores [i]);
				score++;
			}
		}
		GameObject[] enemy = GameObject.FindGameObjectsWithTag ("enemy");
		for (int i = 0; i < enemy.Length; i++) {
			if (enemy [i].transform.position == transform.position) {
				Destroy (enemy [i]);
				lives--;
			}
		}
		if (Input.GetKey ("d"))
		{
			detectedInput = 1;
		}

		if (Input.GetKey ("a"))
		{
			detectedInput = 2;
		}

		beatLength -= Time.deltaTime;
		if (beatLength <= 0)
		{
			if (detectedInput == 1 && transform.position.x < trackLength) {
				transform.Translate (new Vector3 (3f, 0f, 0f));
			} else if (detectedInput == 2 && transform.position.x > (-1 * trackLength)) {
				transform.Translate (new Vector3 (-3f, 0f, 0f));
			}
			detectedInput = 0;
			beatLength = 1f;
		}
	}
}
