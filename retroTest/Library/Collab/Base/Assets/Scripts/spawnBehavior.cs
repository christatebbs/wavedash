﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SynchronizerData;
using System.Linq;

public class spawnBehavior : MonoBehaviour {

	public int laneNumbers = 3;
	private int currentPhase = 1;
	private int lengthCurPhase;
	private int pathIndex;
	public List<GameObject> availableEnemies;
	public List<GameObject> availableSideEnemies;
	public List<GameObject> availableHealth;
	public List<GameObject> availableLasers;
	public List<GameObject> availableShields;
	public List<GameObject> availableScores1;
	public List<GameObject> availableScores2;
	public List<GameObject> availableScores3;
	public bool multiplayer = false;
	public int spawnerDistance = 21;
	private BeatObserver beatObserver;
	// Use this for initialization
	void Start () {
		availableEnemies = GameObject.FindGameObjectsWithTag ("enemy").ToList();
		availableHealth = GameObject.FindGameObjectsWithTag ("health").ToList();
		availableLasers = GameObject.FindGameObjectsWithTag ("laser").ToList();
		availableShields = GameObject.FindGameObjectsWithTag ("shield").ToList();
		availableScores1 = GameObject.FindGameObjectsWithTag ("scores1").ToList();
		availableScores2 = GameObject.FindGameObjectsWithTag ("scores2").ToList();
		availableScores3 = GameObject.FindGameObjectsWithTag ("scores3").ToList();
		availableSideEnemies = GameObject.FindGameObjectsWithTag ("sideToSideEnemy").ToList();

		Debug.Log (availableEnemies.Count);
		GameObject downObject = GameObject.Find ("down_beat_counter");
		GameObject offObject = GameObject.Find ("off_beat_counter");
		GameObject patternObject = GameObject.Find ("pattern_counter");
		GameObject upObject = GameObject.Find ("up_beat_counter");
		BeatCounter downBeat = downObject.GetComponent<BeatCounter> ();
		BeatCounter offBeat = offObject.GetComponent<BeatCounter> ();
		PatternCounter patternBeat = patternObject.GetComponent<PatternCounter> ();
		BeatCounter upBeat = upObject.GetComponent<BeatCounter> ();
		GameObject[] tempObservers;
		if (!multiplayer) {
			tempObservers = new GameObject[availableEnemies.Count + availableSideEnemies.Count + availableHealth.Count + availableLasers.Count + availableShields.Count +
			                             availableScores1.Count + availableScores2.Count + availableScores3.Count + 3];

			tempObservers [0] = GameObject.Find ("Player");
			tempObservers [1] = GameObject.Find ("laneSpawner");
			tempObservers [2] = GameObject.Find ("timer");
			List<GameObject> tempList = availableEnemies;
			tempList.AddRange (availableHealth);
			tempList.AddRange (availableSideEnemies);
			tempList.AddRange (availableLasers);
			tempList.AddRange (availableShields);
			tempList.AddRange (availableScores1);
			tempList.AddRange (availableScores2);
			tempList.AddRange (availableScores3);
			//Debug.Log (tempList.Count);
			//Debug.Log (tempObeservers.Length);
			for (int i = 0; i < tempObservers.Length - 3; i++) {
				tempObservers [i + 3] = tempList [0];
				tempList.RemoveAt (0);
			}
		} else {
			tempObservers = new GameObject[availableEnemies.Count + availableSideEnemies.Count + availableHealth.Count + availableLasers.Count + availableShields.Count +
				availableScores1.Count + availableScores2.Count + availableScores3.Count + 4];

			tempObservers [0] = GameObject.Find ("Player1");
			tempObservers [1] = GameObject.Find ("Player2");
			tempObservers [2] = GameObject.Find ("laneSpawner");
			tempObservers [3] = GameObject.Find ("timer");
			List<GameObject> tempList = availableEnemies;
			tempList.AddRange (availableHealth);
			tempList.AddRange (availableSideEnemies);
			tempList.AddRange (availableLasers);
			tempList.AddRange (availableShields);
			tempList.AddRange (availableScores1);
			tempList.AddRange (availableScores2);
			tempList.AddRange (availableScores3);
			//Debug.Log (tempList.Count);
			//Debug.Log (tempObeservers.Length);
			for (int i = 0; i < tempObservers.Length - 4; i++) {
				tempObservers [i + 4] = tempList [0];
				tempList.RemoveAt (0);
			}
		}
		availableEnemies = GameObject.FindGameObjectsWithTag ("enemy").ToList();
		availableHealth = GameObject.FindGameObjectsWithTag ("health").ToList();
		availableLasers = GameObject.FindGameObjectsWithTag ("laser").ToList();
		availableShields = GameObject.FindGameObjectsWithTag ("shield").ToList();
		availableScores1 = GameObject.FindGameObjectsWithTag ("scores1").ToList();
		availableScores2 = GameObject.FindGameObjectsWithTag ("scores2").ToList();
		availableScores3 = GameObject.FindGameObjectsWithTag ("scores3").ToList();
		availableSideEnemies = GameObject.FindGameObjectsWithTag ("sideToSideEnemy").ToList();

		downBeat.observers = tempObservers;
		offBeat.observers = tempObservers;
		patternBeat.observers = tempObservers;
		upBeat.observers = tempObservers;
		
		//Debug.Log (upBeat.observers.Length);
		pathIndex = Random.Range (0, laneNumbers);
		lengthCurPhase = 2;
		beatObserver = GetComponent<BeatObserver>();
	}

	// Update is called once per frame
	void Update () {
		if ((beatObserver.beatMask & BeatType.OnBeat) == BeatType.OnBeat) {
			int[] laneSpawn = new int[laneNumbers];	// holds data of what to spawn
			int pathSpawn = Random.Range (0, 100);	// determine what to spawn on path
			if (pathSpawn < 40) {
				laneSpawn [pathIndex] = 0;
			} else if (pathSpawn < 80) {
				laneSpawn [pathIndex] = 1;
			} else if (pathSpawn < 95) {
				laneSpawn [pathIndex] = 2;
			} else {
				laneSpawn [pathIndex] = 3;
			}
			// this always guarantees that the player will be able to move to safe space

			//fill in rest of lanes
			for (int i = 0; i < laneNumbers; i++) {
				if (i != pathIndex) {

					// lanes are filled in with the current phases algorithm
					if (currentPhase == 0) {
						laneSpawn [i] = generateRandom ();
					} else if (currentPhase == 1) {
						laneSpawn [i] = generateEnemies ();
					} else if (currentPhase == 2) {
						laneSpawn [i] = generatePoints ();
					} else if (currentPhase == 3) {
						laneSpawn [i] = generateGoods ();
					} else if (currentPhase == 4) {
						laneSpawn [i] = generateMinimal ();
					} else if (currentPhase == 5) {
						laneSpawn [i] = generateMinimalEnemies ();
					} else if (currentPhase == 6) { 
						if (i != laneNumbers - 1) { 
							laneSpawn [i] = 9; 
						}
					}
				}
			}

			if (currentPhase == 7) { 
				bool atPath = true; 
				int randomNum = 0; 
				while (atPath) {
					randomNum = Random.Range (0, laneNumbers); 
					if (randomNum != pathIndex) { 
						atPath = false; 
					}
				}
				laneSpawn [randomNum] = 8; 
			}

			// spawns lane
			for (int i = 0; i < laneSpawn.Length; i++) {
				float spawnType = Random.Range (0f, 100f);
				if (laneSpawn [i] == 7) {
					GameObject curGo = availableLasers [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableLasers.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (laneSpawn [i] == 6) {
					GameObject curGo = availableShields [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableShields.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (laneSpawn [i] == 5) {
					GameObject curGo = availableHealth [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableHealth.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (laneSpawn [i] == 4) {
					int rand = Random.Range (0, availableEnemies.Count);
					GameObject curGo = availableEnemies [rand];
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableEnemies.RemoveAt (rand);
				} else if (laneSpawn [i] == 3) {
					GameObject curGo = availableScores3 [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableScores3.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (laneSpawn [i] == 2) {
					GameObject curGo = availableScores2 [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableScores2.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (laneSpawn [i] == 1) {
					GameObject curGo = availableScores1 [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableScores1.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				} else if (laneSpawn [i] == 8) { 
					GameObject go = (GameObject)Instantiate (Resources.Load ("movingEnemy"));
					go.transform.position = transform.position;
					go.transform.Translate (new Vector3 ((3 * i), 0, 0));
					go.GetComponent<teleport> ().currentLane = i;
					go.GetComponent<teleport> ().safePath = pathIndex; 
					go.GetComponent<teleport> ().lanes = laneNumbers; 
				} else if (laneSpawn [i] == 9) { 
					GameObject curGo = availableSideEnemies [0];
					curGo.GetComponent<collectMovement> ().currentlyActive = true;
					availableSideEnemies.RemoveAt (0);
					curGo.transform.position = transform.position;
					curGo.transform.Translate (new Vector3 ((3 * i), 0, 0));
				}
			}
			if (multiplayer) {
				for (int i = 0; i < laneSpawn.Length; i++) {
					float spawnType = Random.Range (0f, 100f);
					if (laneSpawn [i] == 7) {
						GameObject curGo = availableLasers [0];
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableLasers.RemoveAt (0);
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
					} else if (laneSpawn [i] == 6) {
						GameObject curGo = availableShields [0];
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableShields.RemoveAt (0);
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
					} else if (laneSpawn [i] == 5) {
						GameObject curGo = availableHealth [0];
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableHealth.RemoveAt (0);
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
					} else if (laneSpawn [i] == 4) {
						int rand = Random.Range (0, availableEnemies.Count);
						GameObject curGo = availableEnemies [rand];
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableEnemies.RemoveAt (rand);
					} else if (laneSpawn [i] == 3) {
						GameObject curGo = availableScores3 [0];
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableScores3.RemoveAt (0);
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
					} else if (laneSpawn [i] == 2) {
						GameObject curGo = availableScores2 [0];
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableScores2.RemoveAt (0);
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
					} else if (laneSpawn [i] == 1) {
						GameObject curGo = availableScores1 [0];
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableScores1.RemoveAt (0);
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
					} else if (laneSpawn [i] == 8) { 
						GameObject go = (GameObject)Instantiate (Resources.Load ("movingEnemy"));
						go.transform.position = transform.position;
						go.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
						go.GetComponent<teleport> ().currentLane = i;
						go.GetComponent<teleport> ().safePath = pathIndex; 
						go.GetComponent<teleport> ().lanes = laneNumbers; 
					} else if (laneSpawn [i] == 9) { 
						GameObject curGo = availableSideEnemies [0];
						curGo.GetComponent<collectMovement> ().currentlyActive = true;
						availableSideEnemies.RemoveAt (0);
						curGo.transform.position = transform.position;
						curGo.transform.Translate (new Vector3 ((3 * i) + spawnerDistance, 0, 0));
					}
				}
			}
				
			int nextPath = 0;	// determine if path with go left, right or straight next beat
			if (pathIndex == 0) {
				nextPath = Random.Range (0, 2);
			} else if (pathIndex == laneNumbers - 1) {
				nextPath = Random.Range (-1, 1);
			} else {
				nextPath = Random.Range (-1, 2);
			}
			pathIndex = pathIndex + nextPath;	// move the path of index
			lengthCurPhase = lengthCurPhase - 1;
			if (lengthCurPhase == 0) {
				currentPhase = Random.Range (0, 7);	// change current phase
				if (currentPhase == 0) {
					lengthCurPhase = 3;
				} else if (currentPhase == 1) {
					lengthCurPhase = 10;
				} else if (currentPhase == 2) {
					lengthCurPhase = 2;
				} else if (currentPhase == 3) {
					lengthCurPhase = 1;
				} else if (currentPhase == 4) {
					lengthCurPhase = 2;
				} else if (currentPhase == 5) {
					lengthCurPhase = 10;
				} else if (currentPhase == 6) { 
					lengthCurPhase = 2; 
				} else if (currentPhase == 7) { 
					lengthCurPhase = 2; 
				}
			}
		}
	}
		
	// 0 = blanks
	// 1 = score1
	// 2 = score2
	// 3 = score3	
	// 4 = enemy
	// 5 = health
	// 6 = shield
	// 7 = laser
	// 8 = moving enemy 

	int generateRandom() {
		float spawnType = Random.Range (0f, 100f);
		if (spawnType < 5f) {
			return 7;
		} else if (spawnType < 10f) {
			return 6;      //shield 
		} else if (spawnType < 15f) {
			return 5;      //health
		} else if (spawnType < 40f) {
			return 4;      //enemy
		} else if (spawnType < 45f) {
			return 3;      //score3
		} else if (spawnType < 60f) {
			return 2;      //score2
		} else if (spawnType < 80f) {
			return 1;      //score1
		} else {
			return 0;	   //blanks
		}
	}

	int generateEnemies() {
		float spawnType = Random.Range (0f, 100f);
		if (spawnType < 10f) {
			return 0;      //blanks
		} else {
			return 4;	//enemy
		}
	}

	int generatePoints() {
		float spawnType = Random.Range (0f, 100f);
		if (spawnType < 33f) {
			return 1;      //score1 
		} else if (spawnType < 66f) {
			return 2;      //score2
		} else if (spawnType < 90f) {
			return 3;      //score3
		} else {
			return 0;	//blanks
		}
	}

	int generateGoods() {
		float spawnType = Random.Range (0f, 100f);
		if (spawnType < 20f) {
			return 7;      //laser 
		} else if (spawnType < 40f) {
			return 6;      //shield
		} else if (spawnType < 45f) {
			return 5;      //health
		} else {
			return 4;	//blanks
		}
	}

	int generateMinimal() {
		float spawnType = Random.Range (0f, 100f);
		if (spawnType < 40f) {
			return 1;      //score1 
		} else if (spawnType < 10f) {
			return 2;      //score2
		} else {
			return 0;	//blanks
		}
	}

	int generateMinimalEnemies() {
		float spawnType = Random.Range (0f, 100f);
		if (spawnType < 50f) {
			return 4;      //blanks
		} else if (spawnType < 60f) {
			return 7;	//laser
		} else if (spawnType < 70f) {
			return 6;	//enemy
		} else {
			return 0;
		}
	}
}
