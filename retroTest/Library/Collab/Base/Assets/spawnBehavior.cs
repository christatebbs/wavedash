﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBehavior : MonoBehaviour {

	public float beatLength = 1.0f;
	private float beatTimer;
	public int laneNumbers = 3;
	public int burstLength = 3;
	private int currentPhase = 2;
	private int currentBurstCount = 0;
	private int[,] burst;
	// Use this for initialization
	void Start () {
		beatTimer = beatLength;
		burst = new int[laneNumbers, burstLength];
		if (currentPhase == 0) {
			burst = generatePointBurst ();
		} else if (currentPhase == 1) {
			burst = generateEnemyBurst ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		beatTimer -= Time.deltaTime;
		if (beatTimer <= 0) {
			if (currentBurstCount == burstLength) {
				burst = new int[laneNumbers, burstLength];
				if (currentPhase == 0) {
					Debug.Log ("points generated");
					burst = generatePointBurst ();
				} else if (currentPhase == 1) {
					Debug.Log ("enemy generated");
					burst = generateEnemyBurst ();
				} else if (currentPhase == 2) {
					Debug.Log ("wall generated");
					burst = generateWallBurst ();
				}
				currentBurstCount = 0;
			}

			GameObject[] enemies = GameObject.FindGameObjectsWithTag ("enemy");
			bool[] validLaneChange = new bool[laneNumbers];
			for (int i = 0; i < validLaneChange.Length; i++) {
				Vector3 forward = new Vector3 ((transform.position.x + (i * 3)), transform.position.y, (transform.position.z - 3));
				Vector3 left = new Vector3 ((transform.position.x + ((i - 1) * 3)), transform.position.y, (transform.position.z - 3));
				Vector3 right = new Vector3 ((transform.position.x + ((i + 1) * 3)), transform.position.y, (transform.position.z - 3));
				bool[] isValid = new bool[3];
				isValid [0] = true;
				isValid [1] = true;
				isValid [2] = true;
				for (int j = 0; j < enemies.Length; j++) {
					if (enemies [j].transform.position.Equals (forward)) {
						isValid [0] = false;
					}
					if (enemies [j].transform.position.Equals (left) || i == 0) {
						isValid [1] = false;
					}
					if (enemies [j].transform.position.Equals (right) || i == validLaneChange.Length - 1) {
						isValid [2] = false;
					}
				}
				for (int j = 0; j < isValid.Length; j++) {
					if (isValid [j]) {
						validLaneChange [i] = true;
					}
				}
			}
			bool laneGoodToGo = false;
			// filtering
			for (int i = 0; i < validLaneChange.Length; i++) {
				if (validLaneChange[i] && burst[i, currentBurstCount] == 0) {
					laneGoodToGo = true;
				}
			}
			if (!laneGoodToGo && currentPhase != 2) {
				int rand = Random.Range (0, laneNumbers - 1);
				while (!validLaneChange [rand]) {
					rand = Random.Range (0, laneNumbers - 1);
				}
				//Debug.Log ("change was made");
				burst [rand, currentBurstCount] = 0;
			}

			// evaluate burst and make apropriate adjustments, when dealing with enemies in a burst, blank spaces must always be adjacent 
			for (int i = 0; i < laneNumbers; i++) {
				if (burst [i, currentBurstCount] != 0) {
					float spawnType = Random.Range (0f, 100f);
					if (burst [i, currentBurstCount] == 6) {
						GameObject go = (GameObject)Instantiate (Resources.Load ("shield"));
						go.transform.position = transform.position;
						go.transform.Translate (new Vector3 ((3 * i), 0, 0));
					} else if (burst [i, currentBurstCount] == 5) {
						GameObject go = (GameObject)Instantiate (Resources.Load ("health"));
						go.transform.position = transform.position;
						go.transform.Translate (new Vector3 (0, (-3 * i), 0));
					} else if (burst [i, currentBurstCount] == 4) {
						GameObject go = (GameObject)Instantiate (Resources.Load ("enemy"));
						go.transform.position = transform.position;
						go.transform.Translate (new Vector3 ((3 * i), 0, 0));
					} else if (burst [i, currentBurstCount] == 3) {
						GameObject go = (GameObject)Instantiate (Resources.Load ("scores3"));
						go.transform.position = transform.position;
						go.transform.Translate (new Vector3 ((3 * i), 0, 0));
					} else if (burst [i, currentBurstCount] == 2) {
						GameObject go = (GameObject)Instantiate (Resources.Load ("scores2"));
						go.transform.position = transform.position;
						go.transform.Translate (new Vector3 ((3 * i), 0, 0));
					} else if (burst [i, currentBurstCount] == 1) {
						GameObject go = (GameObject)Instantiate (Resources.Load ("scores1"));
						go.transform.position = transform.position;
						go.transform.Translate (new Vector3 ((3 * i), 0, 0));
					}
				}
			}
			currentBurstCount++;
			beatTimer = beatLength;
		}
	}
	// 0 = blanks
	// 1 = score1
	// 2 = score2
	// 3 = score3
	// 4 = enemy
	// 5 = health
	// 6 = shield
	int[,] generatePointBurst() {
		int[,] burst = new int[laneNumbers, burstLength];
		for (int i = 0; i < laneNumbers; i++) {
			for (int j = 0; j < burstLength; j++) {
				float spawnType = Random.Range (0f, 100f);
				if (spawnType < 5f) {
					burst [i, j] = 6;      //shield 
				} else if (spawnType < 10f) {
					burst [i, j] = 5;      //health
				} else if (spawnType < 15f) {
					burst [i, j] = 4;      //enemy
				} else if (spawnType < 30f) {
					burst [i, j] = 3;      //score3
				} else if (spawnType < 50f) {
					burst [i, j] = 2;      //score2
				} else if (spawnType < 70f) {
					burst [i, j] = 1;      //score1
				} else {
					burst [i, j] = 0;	   //blanks
				}
			}
		}
		return burst;
	}

	int[,] generateEnemyBurst() {
		int[,] burst = new int[laneNumbers, burstLength];
		for (int i = 0; i < laneNumbers; i++) {
			for (int j = 0; j < burstLength; j++) {
				float spawnType = Random.Range (0f, 100f);
				if (spawnType < 5f) {
					burst [i, j] = 6;      //shield 
				} else if (spawnType < 40f) {
					burst [i, j] = 4;      //enemy
				} else if (spawnType < 45f) {
					burst [i, j] = 3;      //score3
				} else if (spawnType < 55f) {
					burst [i, j] = 2;      //score2
				} else if (spawnType < 70f) {
					burst [i, j] = 1;      //score1
				} else {
					burst [i, j] = 0;      //blanks
				}
			}
		}
		return burst;
	}

	// 0 = blanks
	// 1 = score1
	// 2 = score2
	// 3 = score3
	// 4 = enemy
	// 5 = health
	// 6 = shield
	int[,] generateWallBurst() {
		int[,] burst = new int[laneNumbers, burstLength];
		if (laneNumbers == 3) {
			return new int[,] { {6, 4, 4}, {4, 0, 4}, {0, 4, 4} };
		} else if (laneNumbers == 4) {
			
		}
		return burst;
	}
}
